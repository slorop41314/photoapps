/**
 * @format
 */

import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './src/Screens/App';
import ReduxWrapper from './src/Navigation/ReduxWrapper';

require('./src/Config/ReactotronConfig');

AppRegistry.registerComponent(appName, () => ReduxWrapper(App));
