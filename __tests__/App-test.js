/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../src/Screens/App';

// Note: test renderer must be required after react-native.
import renderer, {act} from 'react-test-renderer';
import {Provider} from 'react-redux';
import createStore from '../src/Redux';
import configureStore from 'redux-mock-store';

jest.useFakeTimers();
const mockStore = configureStore([]);

it('renders app correctly', () => {
  const createdStore = createStore();
  const store = mockStore({...createdStore.getState()});
  renderer.create(
    <Provider store={store}>
      <App />
    </Provider>
  );
});
