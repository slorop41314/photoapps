module.exports = function (api) {
  api.cache(true);
  if (
    process.env.NODE_ENV === 'production' ||
    process.env.BABEL_ENV === 'production'
  ) {
    return {
      presets: ['module:metro-react-native-babel-preset'],
      plugins: [
        'ignite-ignore-reactotron',
        'transform-remove-console'
        // 'react-native-reanimated/plugin'
      ]
    };
  }
  return {
    presets: ['module:metro-react-native-babel-preset']
    // plugins: ['react-native-reanimated/plugin']
  };
};
