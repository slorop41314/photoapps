module.exports = {
  preset: 'react-native',
  setupFilesAfterEnv: [
    '<rootDir>/__mocks__/setupMockLibraries.js',
    './node_modules/react-native-gesture-handler/jestSetup.js',
    './jest.setup.js'
  ],
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '\\.(css|less)$': 'identity-obj-proxy'
  },
  transformIgnorePatterns: [
    'node_modules/(?!(react-native' +
      '|react-navigation-tabs' +
      '|react-native-iphone-x-helper' +
      '|react-native-screens' +
      '|react-native-mmkv-storage' +
      '|@react-native-firebase/messaging' +
      '|@react-native-firebase/app' +
      // '|@react-native-firebase/dynamic-linking' +
      // '|react-native-gesture-handler' +
      '|@sentry/react-native' +
      ')/)'
  ],
  globals: {
    __DEV__: true
  }
};
