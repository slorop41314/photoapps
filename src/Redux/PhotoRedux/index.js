import {createReducer, createActions} from 'reduxsauce';
import {DEFAULT_FETCHING_STATE} from '../../Constant/CommonState';
import R from 'ramda';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  getPhotosDataRequest: null,
  getPhotosDataSuccess: ['payload'],
  getPhotosDataFailure: null,

  addNewData: ['data'],
  editData: ['data'],
  removeData: ['id']
});

export const PhotoTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  getDataState: DEFAULT_FETCHING_STATE,
  album: [],
  data: {}
};

/* ------------- Selectors ------------- */

export const PhotoSelectors = {
  selectData: ({photos}) => photos.data,
  selectAlbum: ({photos}) => photos.album,
  selectDataState: ({photos}) => photos.getDataState,

  selectSingleData: ({photos}, props) => {
    const id = props.route.params.id;
    return photos.data[id];
  }
};

/* ------------- Reducers ------------- */

export const getPhotosDataRequestReducer = (state) => {
  return {
    ...state,
    fetching: true
  };
};

export const getPhotosDataSuccessReducer = (state, {payload}) => {
  let albumList = [...new Set(payload.map((item) => item.albumId))];
  albumList.unshift('All');
  const objectPayload = payload.reduce(
    (obj, item) => ({
      ...obj,
      [item.id]: item
    }),
    {}
  );
  return {
    ...state,
    fetching: false,
    error: false,
    data: objectPayload,
    album: albumList
  };
};

export const getPhotosDataFailureReducer = (state) => {
  return {
    ...state,
    error: true,
    fetching: false
  };
};

export const addDataReducer = (state, payload) => {
  const {data} = payload;
  return {
    ...state,
    data: {
      ...state.data,
      [data.id]: data
    }
  };
};

export const editDataReducer = (state, payload) => {
  const {data} = payload;
  return {
    ...state,
    data: {
      ...state.data,
      [data.id]: data
    }
  };
};

export const removeDataReducer = (state, payload) => {
  const {id} = payload;
  return {
    ...state,
    data: R.dissoc([id], state.data)
  };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PHOTOS_DATA_REQUEST]: getPhotosDataRequestReducer,
  [Types.GET_PHOTOS_DATA_SUCCESS]: getPhotosDataSuccessReducer,
  [Types.GET_PHOTOS_DATA_FAILURE]: getPhotosDataFailureReducer,

  [Types.EDIT_DATA]: editDataReducer,

  [Types.ADD_NEW_DATA]: addDataReducer,
  [Types.REMOVE_DATA]: removeDataReducer
});
