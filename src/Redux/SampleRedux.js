import {createReducer, createActions} from 'reduxsauce';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  actionRequest: ['data'],
  actionSuccess: ['payload'],
  actionFailure: null,
  reset: null
});

export const SampleTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  action: {
    fetching: false,
    data: undefined,
    payload: undefined,
    error: undefined
  }
};

/* ------------- Selectors ------------- */

export const SampleSelectors = {
  selectAction: ({action}) => action.action
};

/* ------------- Reducers ------------- */

export const actionRequestReducer = (state, {data}) => {
  return {
    ...state.action,
    fetching: true,
    data
  };
};

export const actionSuccessReducer = (state, {payload}) => {
  return {
    ...state.action,
    fetching: false,
    payload,
    error: undefined
  };
};

export const actionFailureReducer = (state) => {
  return {
    ...state.action,
    fetching: false,
    error: true
  };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ACTION_REQUEST]: actionRequestReducer,
  [Types.ACTION_SUCCESS]: actionSuccessReducer,
  [Types.ACTION_FAILURE]: actionFailureReducer
});
