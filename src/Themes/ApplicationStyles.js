import {StyleSheet} from 'react-native';
import Colors from './Colors';

const ApplicationStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  sectionPadding: {
    padding: 12
  },
  sectionHorizontalPadding: {
    paddingHorizontal: 12
  },
  bigImage: {
    height: 200,
    width: '100%',
    resizeMode: 'cover'
  }
});

export default ApplicationStyles;
