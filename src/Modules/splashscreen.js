import {NativeModules, Platform} from 'react-native';

const RNSplashScreen = NativeModules.SplashScreen;

function show() {
  if (Platform.OS === 'android') {
    RNSplashScreen.show();
  } else {
    console.log(
      'not available for ios, create on xcode launchscreen.storyboard'
    );
  }
}

function hide() {
  if (Platform.OS === 'android') {
    RNSplashScreen.hide();
  } else {
    console.log(
      'not available for ios, create on xcode launchscreen.storyboard'
    );
  }
}

export default {
  show,
  hide
};
