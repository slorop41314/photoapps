import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator
} from '@react-navigation/stack';
import SplashScreen from '../Screens/SplashScreen';
import NavigationServices, {
  navigationRef,
  NAVIGATION_NAME
} from './NavigationServices';
import {Platform, Pressable, Text} from 'react-native';
import RootedScreen from '../Screens/Error/RootedScreen';
import HomeScreen from '../Screens/Main/HomeScreen';
import DetailScreen from '../Screens/Main/DetailScreen';
import {Fonts} from '../Themes';
import CreateScreen from '../Screens/Main/CreateScreen';

const Stack = createStackNavigator();

const defaultScreenAnimations = {
  cardStyleInterpolator:
    Platform.OS === 'ios'
      ? CardStyleInterpolators.forHorizontalIOS
      : CardStyleInterpolators.forRevealFromBottomAndroid,
  headerBackTitleVisible: false
};

const hideHeaderOptions = {
  headerShown: false
};

function AppNavigator() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{...defaultScreenAnimations}}>
        <Stack.Screen
          name={NAVIGATION_NAME.SPLASH}
          component={SplashScreen}
          options={hideHeaderOptions}
        />
        {/* Error/Forbidden Screen */}
        <Stack.Screen
          name={NAVIGATION_NAME.ERROR.rooted}
          component={RootedScreen}
          options={hideHeaderOptions}
        />
        {/* Main Screen */}
        <Stack.Screen
          name={NAVIGATION_NAME.MAIN.home}
          component={HomeScreen}
          options={{
            title: 'Home',
            headerRight: () => {
              return (
                <Pressable
                  style={{paddingHorizontal: 12}}
                  onPress={() =>
                    NavigationServices.navigate(NAVIGATION_NAME.MAIN.new)
                  }>
                  <Text style={Fonts.style.h4}>+</Text>
                </Pressable>
              );
            }
          }}
        />
        <Stack.Screen
          name={NAVIGATION_NAME.MAIN.new}
          component={CreateScreen}
          options={{title: 'New Data'}}
        />
        <Stack.Screen
          name={NAVIGATION_NAME.MAIN.detail}
          component={DetailScreen}
          options={{title: 'Detail'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;
