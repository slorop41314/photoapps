/**
 * This services used to help you handle navigation easyly
 * put function that help you navigate here.
 */

import {StackActions} from '@react-navigation/native';
import * as React from 'react';

export const navigationRef = React.createRef();

export const NAVIGATION_NAME = {
  APP: 'app',
  SPLASH: 'splash',
  MAIN: {
    home: 'main.home',
    detail: 'main.detail',
    new: 'main.new'
  },
  ERROR: {
    rooted: 'error.rooted'
  }
};

function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

function dispatch(action) {
  navigationRef.current?.dispatch(action);
}

function pop() {
  navigationRef.current?.goBack();
}

function popToTop() {
  const action = StackActions.popToTop();
  navigationRef.current?.dispatch(action);
}

function replace(routeName, params) {
  const action = StackActions.replace(routeName, params);
  navigationRef.current?.dispatch(action);
}

function push(routeName, params) {
  const action = StackActions.push(routeName, params);
  navigationRef.current?.dispatch(action);
}

export default {
  navigate,
  dispatch,
  pop,
  popToTop,
  replace,
  push
};
