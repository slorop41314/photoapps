const NotificationEvent = {
  type: {
    onNotification: 'on-notification',
    onInitial: 'on-initial',
    onOpen: 'on-open',
    onError: 'on-error',
    tokenRefresh: 'token-refresh',
    tokenGet: 'token-get'
  },
  action: {
    // Pubnub
    message: 'message'
  }
};

export default NotificationEvent;
