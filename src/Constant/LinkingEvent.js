const LinkingEvent = {
  type: {
    onLink: 'on-link',
    onInitial: 'on-initial',
    onError: 'on-error'
  }
};

export default LinkingEvent;
