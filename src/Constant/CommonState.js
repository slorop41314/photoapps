const DEFAULT_FETCHING_STATE = {
  data: undefined,
  fetching: false,
  payload: undefined,
  error: false
};

// For pagination used
const DEFAULT__PG_FETCHING_STATE = {
  data: undefined,
  fetching: false,
  meta: {
    page: 0,
    per_page: 10,
    total_page: 1,
    total_count: 0
  },
  payload: undefined,
  error: false
};

export {DEFAULT_FETCHING_STATE, DEFAULT__PG_FETCHING_STATE};
