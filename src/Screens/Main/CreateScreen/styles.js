import {Platform, StyleSheet} from 'react-native';
import {Colors} from '../../../Themes';

const styles = StyleSheet.create({
  inputContainer: {
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 5,
    minWidth: 100,
    padding: Platform.OS === 'ios' ? 6 : 0
  },
  photoContainer: {
    height: 200,
    borderWidth: 1,
    borderColor: Colors.border,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
