import React, {Component} from 'react';
import {
  Platform,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {ApplicationStyles} from '../../../Themes';
import {Button, Gap, Input} from '../../../Components';
import {launchImageLibrary} from 'react-native-image-picker';
import RNPickerSelect from 'react-native-picker-select';
import styles from './styles';

// Redux
import {connect} from 'react-redux';
import PhotoActions, {PhotoSelectors} from '../../../Redux/PhotoRedux';
import {createSelector} from 'reselect';

class CreateScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      url: null,
      albumId: null
    };

    this.saveNewData = this.saveNewData.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.pickImage = this.pickImage.bind(this);
  }

  saveNewData = () => {
    const {addDataRequest, navigation} = this.props;
    const {title, url, albumId} = this.state;
    const newData = {
      id: 100 + Math.ceil(Math.random() * 10000),
      thumbnailUrl: url,
      url,
      title,
      albumId
    };
    addDataRequest(newData);
    navigation.goBack();
  };

  onChangeTitle = (text) => {
    this.setState({title: text});
  };

  pickImage() {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 200,
        maxWidth: 200
      },
      (response) => {
        if (!response.didCancel) {
          this.setState({url: response.uri});
        }
      }
    );
  }

  render() {
    const {title, url, albumId} = this.state;
    const {albumData} = this.props;
    const filteredData = albumData
      .filter((item) => item !== 'All')
      .map((item) => {
        return {label: `${item}`, value: item};
      });
    return (
      <SafeAreaView style={ApplicationStyles.container}>
        <View style={ApplicationStyles.sectionPadding}>
          <Text>Title</Text>
          <Input value={title} onChangeText={this.onChangeTitle} />
          <Gap height={12} />
          <Text>Album</Text>
          <View style={styles.inputContainer}>
            <RNPickerSelect
              style={{inputAndroid: {color: 'black'}}}
              onValueChange={(value) => {
                if (value) {
                  this.setState({albumId: value});
                }
              }}
              value={albumId}
              items={filteredData}
            />
          </View>
          <Gap height={12} />
          <Text>Photo (tap to change)</Text>
          <Gap height={8} />
          <TouchableOpacity
            onPress={this.pickImage}
            style={styles.photoContainer}>
            {url == null ? (
              <Text>Tap to add photo</Text>
            ) : (
              <FastImage
                style={ApplicationStyles.bigImage}
                source={{
                  uri: url,
                  priority: FastImage.priority.normal
                }}
                fallback={Platform.OS === 'android'}
                resizeMode={FastImage.resizeMode.cover}
              />
            )}
          </TouchableOpacity>

          <Gap height={24} />
          <Button
            label="SAVE"
            onPress={this.saveNewData}
            isDisabled={!(url !== null && title !== '' && albumId !== null)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    albumData: getAlbumSelector(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addDataRequest: (newData) => dispatch(PhotoActions.addNewData(newData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateScreen);

const getAlbumSelector = createSelector(
  PhotoSelectors.selectAlbum,
  (data) => data
);
