import React, {Component} from 'react';
import {
  Platform,
  TouchableOpacity,
  SafeAreaView,
  Text,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {ApplicationStyles, Colors, Fonts} from '../../../Themes';
import {Button, Gap, Input} from '../../../Components';
import {launchImageLibrary} from 'react-native-image-picker';

// Redux
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import PhotoActions, {PhotoSelectors} from '../../../Redux/PhotoRedux';

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    const {title, url} = props.photo;
    this.state = {
      title,
      url
    };

    this.saveNewData = this.saveNewData.bind(this);
    this.removeData = this.removeData.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.pickImage = this.pickImage.bind(this);
  }

  saveNewData = () => {
    const {editDataRequest, photo, navigation} = this.props;
    editDataRequest({
      ...photo,
      ...this.state
    });
    navigation.goBack();
  };

  removeData = () => {
    const {removeDataRequest, photo, navigation} = this.props;
    removeDataRequest(photo.id);
    navigation.goBack();
  };

  onChangeTitle = (text) => {
    this.setState({title: text});
  };

  pickImage() {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 200,
        maxWidth: 200
      },
      (response) => {
        if (!response.didCancel) {
          this.setState({url: response.uri});
        }
      }
    );
  }

  render() {
    const {title, url} = this.state;
    return (
      <SafeAreaView style={ApplicationStyles.container}>
        <View style={ApplicationStyles.sectionPadding}>
          <Text>Title</Text>
          <Input value={title} onChangeText={this.onChangeTitle} />
          <Gap height={12} />
          <Text>Photo (tap to change)</Text>
          <Gap height={8} />
          <TouchableOpacity onPress={this.pickImage}>
            <FastImage
              style={ApplicationStyles.bigImage}
              source={{
                uri: url,
                priority: FastImage.priority.normal
              }}
              fallback={Platform.OS === 'android'}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableOpacity>
          <Gap height={24} />
          <Button
            label="DELETE DATA"
            buttonColor={Colors.red}
            onPress={this.removeData}
          />
          <Gap height={12} />
          <Button label="SAVE" onPress={this.saveNewData} />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    photo: getPhotoSelector(state, props)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    editDataRequest: (data) => dispatch(PhotoActions.editData(data)),

    removeDataRequest: (id) => dispatch(PhotoActions.removeData(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailScreen);

const getPhotoSelector = createSelector(
  PhotoSelectors.selectSingleData,
  (data) => {
    return data;
  }
);
