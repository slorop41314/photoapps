import React, {Component} from 'react';
import {FlatList, SafeAreaView, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {ApplicationStyles, Fonts} from '../../../Themes';
import RNPickerSelect from 'react-native-picker-select';
// Redux
import {connect} from 'react-redux';
import {createSelector} from 'reselect';
import PhotoActions, {PhotoSelectors} from '../../../Redux/PhotoRedux';
import R from 'ramda';
import styles from './styles';
import CardList from './CardList';

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedAlbum: 'All'
    };

    this.getInitialPhotosData = this.getInitialPhotosData.bind(this);
    this.renderList = this.renderList.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
  }

  componentDidMount() {
    const {photosData} = this.props;
    // Only fetch data when initial open (no data fetch-ed yet)
    // if (photosData.length === 0) {
    this.getInitialPhotosData();
    // }
  }

  getInitialPhotosData = () => {
    const {getPhotoData} = this.props;
    getPhotoData();
  };

  renderList = ({item}) => {
    const {url, title, id} = item;
    return <CardList id={id} url={url} title={title} />;
  };

  renderHeader = () => {
    const {albumData} = this.props;
    const {selectedAlbum} = this.state;
    return (
      <View style={[ApplicationStyles.sectionPadding, styles.headerContent]}>
        <Text style={Fonts.style.h6}>Photos</Text>
        <View style={styles.inputContainer}>
          <RNPickerSelect
            style={styles.inputContainer}
            onValueChange={(value) => {
              if (value) {
                this.setState({selectedAlbum: value});
              }
            }}
            value={selectedAlbum}
            items={albumData.map((item) => {
              return {label: `${item}`, value: item};
            })}
          />
        </View>
      </View>
    );
  };

  render() {
    const {photosData} = this.props;
    const {selectedAlbum} = this.state;
    return (
      <SafeAreaView style={ApplicationStyles.container}>
        <FlatList
          ListHeaderComponent={this.renderHeader}
          data={
            selectedAlbum === 'All'
              ? R.values(photosData)
              : R.values(photosData).filter(
                  (item) => item.albumId === selectedAlbum
                )
          }
          numColumns={2}
          initialNumToRender={12}
          renderItem={this.renderList}
          keyExtractor={(item) => item.id.toString()}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    albumData: getAlbumSelector(state),
    photosData: getInitialPhotosDataSelector(state),
    photosState: getPhotosStateSelector(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPhotoData: () => dispatch(PhotoActions.getPhotosDataRequest())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const getPhotosStateSelector = createSelector(
  PhotoSelectors.selectDataState,
  (data) => data
);

const getInitialPhotosDataSelector = createSelector(
  PhotoSelectors.selectData,
  (data) => data
);

const getAlbumSelector = createSelector(
  PhotoSelectors.selectAlbum,
  (data) => data
);
