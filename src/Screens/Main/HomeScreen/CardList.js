import React from 'react';
import {Platform, TouchableOpacity, Text} from 'react-native';
import FastImage from 'react-native-fast-image';
import NavigationServices, {
  NAVIGATION_NAME
} from '../../../Navigation/NavigationServices';

const CardImage = ({title, url, id}) => {
  return (
    <TouchableOpacity
      style={{flex: 1, marginHorizontal: 8, marginTop: 12}}
      onPress={() => {
        NavigationServices.navigate(NAVIGATION_NAME.MAIN.detail, {id});
      }}>
      <FastImage
        style={{height: 200}}
        source={{
          uri: url,
          priority: FastImage.priority.normal
        }}
        fallback={Platform.OS === 'android'}
        resizeMode={FastImage.resizeMode.cover}
      />
      <Text>{title}</Text>
    </TouchableOpacity>
  );
};

export default React.memo(CardImage);
