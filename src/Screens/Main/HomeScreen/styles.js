import {Platform, StyleSheet} from 'react-native';
import {Colors} from '../../../Themes';

const styles = StyleSheet.create({
  headerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 5,
    minWidth: 100,
    padding: Platform.OS === 'ios' ? 6 : 0
  }
});

export default styles;
