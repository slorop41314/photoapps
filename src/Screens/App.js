/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Fragment, PureComponent} from 'react';
import {StatusBar, View} from 'react-native';
import {connect} from 'react-redux';
import REDUX_PERSIST from '../Config/ReduxPersist';
import splashscreen from '../Modules/splashscreen';
import StartupActions from '../Redux/SampleRedux';
// import DownloadUpdateModal from '../Components/DownloadUpdateModal';
import AppNavigator from '../Navigation';
import * as Sentry from '@sentry/react-native';

class App extends PureComponent {
  componentDidMount() {
    Sentry.init({
      dsn:
        'https://e5576ad0bdf04d8a8f240ab879fba367@o535192.ingest.sentry.io/5654261'
    });
    if (!REDUX_PERSIST.active) {
      const {startup} = this.props;
      startup();
    }
  }

  render() {
    splashscreen.hide();
    return (
      <Fragment>
        <AppNavigator />
        <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
        {/* <DownloadUpdateModal /> */}
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    startup: () => dispatch(StartupActions.startup())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
