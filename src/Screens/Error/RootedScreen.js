import React, {PureComponent, useEffect} from 'react';
import {Button, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
import ApplicationStyles from '../../Themes/ApplicationStyles';

class SplashScreen extends PureComponent {
  render() {
    return (
      <SafeAreaView style={ApplicationStyles.containerCenter}>
        <Text style={styles.text}>We've detect that your phone is rooted</Text>
        <View style={styles.gap} />
        <Text style={styles.text}>
          Currently we are not allowing any rooted device to use our service,
          please use another phone
        </Text>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    paddingHorizontal: 32
  },
  gap: {
    height: 12
  }
});
