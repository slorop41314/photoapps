import React, {PureComponent, useEffect} from 'react';
import {Button, SafeAreaView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import SampleActions from '../Redux/SampleRedux';
import ApplicationStyles from '../Themes/ApplicationStyles';

class SplashScreen extends PureComponent {
  // constructor(props) {
  //   super(props);
  //   // this.state = {}
  // }

  render() {
    return (
      <SafeAreaView style={ApplicationStyles.containerCenter}>
        <Text> Splash Screen </Text>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    reset: () => dispatch(SampleActions.reset())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
