/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import {call, put, take, cancelled, select} from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';

// TODO: - Add dynamic link lib
// import dynamicLinks from '@react-native-firebase/dynamic-links';
import LinkingEvent from '../Constant/LinkingEvent';

export function firebaseLinkingEventHandler() {
  return eventChannel((emit) => {
    // const onLinkCallback = (payload) => {
    //   if (payload != null) {
    //     emit({type: LinkingEvent.type.onLink, payload});
    //   }
    // };

    // const onInitialLinkCallback = (payload) => {
    //   if (payload != null) {
    //     emit({type: LinkingEvent.type.onInitial, payload});
    //   }
    // };

    // const onLinkErrorCallback = (payload) => {
    //   emit({type: LinkingEvent.type.onError, payload});
    // };

    // const onLinkListener = dynamicLinks().onLink(onLinkCallback);

    // dynamicLinks()
    //   .getInitialLink()
    //   .then(onInitialLinkCallback)
    //   .catch(onLinkErrorCallback);

    const unsubscribe = () => {
      // onLinkListener();
    };

    return unsubscribe;
  });
}

function handleLink(linkData) {
  const splittedLink = linkData.url.split('/');
  // const userData = yield select(UserSelectors.getUserFullData);
}

export function* setupLinkingListener() {
  const firebaseLinkingEvent = yield call(firebaseLinkingEventHandler);

  try {
    while (true) {
      try {
        const eventPayload = yield take(firebaseLinkingEvent);
        const {type, payload} = eventPayload;
        switch (type) {
          case LinkingEvent.type.onInitial:
            // yield take(PubnubStoreTypes.SAVE_USER);
            yield* handleLink(payload);
            break;
          case LinkingEvent.type.onLink:
            yield* handleLink(payload);
            break;
          default:
            console.log('LINKING LISTENER', type, payload);
        }
      } catch (err) {
        console.tron.log('LINKING LISTENER ERROR', err);
      }
    }
  } finally {
    if (yield cancelled()) {
      console.tron.error('LINKING LISTENER CANCELED');
    }
  }
}

export function* initDynamicLinking() {
  try {
    yield* setupLinkingListener();
  } catch (error) {
    console.tron.error('Setup linking listener error', error);
  }
}
