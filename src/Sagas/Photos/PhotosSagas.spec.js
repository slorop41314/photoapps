import {runSaga} from '@redux-saga/core';
import {PhotoTypes} from '../../Redux/PhotoRedux';
import Api from '../../Services/Api';
import {getPhotosSagaHandler} from './PhotosSagas';

describe('Photos Saga', () => {
  const api = Api.create();

  describe('Photos', () => {
    it('should call api and dispatch success action', async () => {
      const requestPhotos = jest.spyOn(api, 'photos').mockImplementation(() =>
        Promise.resolve({
          ok: true
        })
      );
      const dispatched = [];
      const result = await runSaga(
        {
          dispatch: (action) => dispatched.push(action)
        },
        () => getPhotosSagaHandler(api)
      );

      expect(requestPhotos).toHaveBeenCalledTimes(1);
      expect(dispatched[0].type).toEqual(PhotoTypes.GET_PHOTOS_DATA_SUCCESS);
      requestPhotos.mockClear();
    });
  });

  it('Should call api and dispatch error action', async () => {
    const requestPhotos = jest
      .spyOn(api, 'photos')
      .mockImplementation(() => Promise.reject());
    const dispatched = [];
    const result = await runSaga(
      {
        dispatch: (action) => dispatched.push(action)
      },
      () => getPhotosSagaHandler(api)
    );

    expect(requestPhotos).toHaveBeenCalledTimes(1);
    expect(dispatched[0].type).toEqual(PhotoTypes.GET_PHOTOS_DATA_FAILURE);
    requestPhotos.mockClear();
  });
});
