import {all, call, put, takeLatest} from 'redux-saga/effects';
import PhotoActions, {PhotoTypes} from '../../Redux/PhotoRedux';

export function* getPhotosSagaHandler(api) {
  try {
    const response = yield call(api.photos);
    if (response.ok) {
      yield put(PhotoActions.getPhotosDataSuccess(response.data));
    } else {
      throw response;
    }
  } catch (error) {
    yield put(PhotoActions.getPhotosDataFailure());
  }
}

function* getPhotosSaga(api) {
  yield takeLatest(
    PhotoTypes.GET_PHOTOS_DATA_REQUEST,
    getPhotosSagaHandler,
    api
  );
}

export {getPhotosSaga};
