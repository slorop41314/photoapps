/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import {
  call,
  put,
  take,
  all,
  cancelled,
  cancel,
  fork,
  select
} from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';
import R from 'ramda';
import {Platform} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import NotificationEvent from '../Constant/NotificationEvent';

function displayNotification(message) {
  const {notification} = message;
  const {body, title} = notification;
  // showDropDownAlert('success', title, body);
}

export function firebaseNotificationEventHandler() {
  return eventChannel((emit) => {
    const onNotificationCallback = (payload) => {
      emit({type: NotificationEvent.type.onNotification, payload});
    };

    const onNotificationOpenCallback = (payload) => {
      emit({type: NotificationEvent.type.onOpen, payload});
    };

    const onInitialNotificationCallback = (payload) => {
      emit({type: NotificationEvent.type.onInitial, payload});
    };

    const onNotificationErrorCallback = (payload) => {
      emit({type: NotificationEvent.type.onError, payload});
    };

    const onRefreshTokenCallback = (payload) => {
      emit({type: NotificationEvent.type.tokenRefresh, payload});
    };

    const onGetTokenCallback = (payload) => {
      emit({type: NotificationEvent.type.tokenGet, payload});
    };

    const onNotificationListener = messaging().onMessage(
      onNotificationCallback
    );
    const onNotificationOpenListener = messaging().onNotificationOpenedApp(
      onNotificationOpenCallback
    );
    const onTokenRefreshListener = messaging().onTokenRefresh(
      onRefreshTokenCallback
    );

    messaging()
      .getInitialNotification()
      .then(onInitialNotificationCallback)
      .catch(onNotificationErrorCallback);

    messaging()
      .getToken()
      .then(onGetTokenCallback)
      .catch(onNotificationErrorCallback);

    const unsubscribe = () => {
      onNotificationListener();
      onNotificationOpenListener();
      onTokenRefreshListener();
    };

    return unsubscribe;
  });
}

export function handleNotification(payload) {
  const {data, notification} = payload;

  let actionType;
  const {android, ios} = notification;
  actionType = Platform.select({
    android: android?.clickAction,
    ios: payload.category
  });

  if (!actionType) {
    actionType = data.click_action;
  }

  switch (actionType) {
    default: {
      break;
    }
  }
}

export function* setupNotificationListener() {
  const firebaseNotificationEvent = yield call(
    firebaseNotificationEventHandler
  );
  try {
    while (true) {
      try {
        // An error from socketChannel will cause the saga jump to the catch block
        const eventPayload = yield take(firebaseNotificationEvent);
        const {type, payload} = eventPayload;
        switch (type) {
          case NotificationEvent.type.onNotification: {
            displayNotification(payload);
            break;
          }
          case NotificationEvent.type.onOpen: {
            yield* handleNotification(payload);
            break;
          }
          case NotificationEvent.type.onInitial: {
            if (payload) {
              // yield take(PubnubStoreTypes.SAVE_USER);
              yield* handleNotification(payload);
            }
            break;
          }
          case NotificationEvent.type.onError: {
            console.tron.log('NOTIFICATION ERROR', payload);
            break;
          }
          case NotificationEvent.type.tokenRefresh:
          case NotificationEvent.type.tokenGet: {
            // PubnubManager.setDeviceToken(payload);
            // yield put(UserActions.setFcmToken(payload));
            break;
          }
          default: {
            break;
          }
        }
      } catch (err) {
        console.tron.log('NOTIFICATION LISTENER ERROR', err);
      }
    }
  } finally {
    if (yield cancelled()) {
      console.tron.error('NOTIFICATION LISTENER CANCELED');
    }
  }
}

export function* initNotification() {
  try {
    const authStatus = yield messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      yield* setupNotificationListener();
    }

    // }
  } catch (error) {
    console.tron.error('NOTIFICATION PERMISSION NOT PERMITTED', error);
  }
}
