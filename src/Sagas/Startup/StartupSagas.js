/* eslint-disable no-undef */
import {fork, put, select, takeLatest} from 'redux-saga/effects';
import NavigationServices, {
  NAVIGATION_NAME
} from '../../Navigation/NavigationServices';
import {StartupTypes} from '../../Redux/StartupRedux';
import {initDynamicLinking} from '../LinkingSagas';
import {initNotification} from '../NotificationSagas';

/**
 * This sagas will called at the first time when app lauch
 * this function used to thandle navigation, or any logic that required
 * at initial stattup such as get get user information, set token, navigation, etc
 * @param {*} action
 */
export function* startupHandler(action) {
  // Notification Listener
  yield fork(initNotification);

  // Linking Listener
  yield fork(initDynamicLinking);

  setTimeout(() => {
    NavigationServices.replace(NAVIGATION_NAME.MAIN.home);
  }, 500);
}

function* startupSaga(api) {
  yield takeLatest(StartupTypes.STARTUP, startupHandler);
}

export {startupSaga};
