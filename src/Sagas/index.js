import {all} from 'redux-saga/effects';

/* ------------- Types ------------- */

import Api from '../Services/Api';
import {getPhotosSaga} from './Photos/PhotosSagas';

/* ------------- Sagas ------------- */

import {startupSaga} from './Startup/StartupSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = Api.create();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // STARTUP
    startupSaga(api),
    getPhotosSaga(api)
  ]);
}
