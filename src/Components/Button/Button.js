import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  TouchableNativeFeedback,
  ActivityIndicator
} from 'react-native';
import {Colors} from '../../Themes';
import PropTypes from 'prop-types';

export const ButtonType = {
  primary: 'primary',
  secondary: 'secondary',
  link: 'link',
  bordered: 'bordered'
};

const Button = ({
  label,
  onPress,
  isLoading,
  isDisabled,
  type,
  buttonColor,
  labelColor,
  style = {}
}) => {
  let additionalStyle = {};
  additionalStyle = style;
  if (buttonColor != null) {
    additionalStyle = {...additionalStyle, backgroundColor: buttonColor};
  }
  if (isDisabled) {
    additionalStyle = {...additionalStyle, backgroundColor: Colors.border};
  }
  return (
    <TouchableNativeFeedback
      onPress={onPress}
      disabled={isLoading || typeof onPress !== 'function' || isDisabled}>
      <View style={[styles.container, additionalStyle]}>
        {isLoading ? (
          <ActivityIndicator color={Colors.white} />
        ) : (
          <Text style={{color: labelColor}}>{label}</Text>
        )}
      </View>
    </TouchableNativeFeedback>
  );
};

export default Button;

Button.propTypes = {
  label: PropTypes.string,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  type: PropTypes.string,
  buttonColor: PropTypes.string,
  labelColor: PropTypes.string,
  style: PropTypes.object
};

Button.defaultProps = {
  label: 'Default title',
  onPress: () => {},
  isLoading: false,
  isDisabled: false,
  type: ButtonType.primary,
  buttonColor: Colors.primary,
  labelColor: Colors.white,
  style: {}
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    padding: 12,
    borderRadius: 5
  }
});
