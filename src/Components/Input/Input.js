import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {Colors} from '../../Themes';

const Input = ({
  placeholder,
  onChangeText,
  value,
  error,
  containerStyle,
  touched
}) => {
  return (
    <View style={containerStyle}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        onChangeText={onChangeText}
        value={value}
      />
      {touched && error && error !== '' && (
        <Text style={styles.errorInput}>{error}</Text>
      )}
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: {
    padding: 12,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.border
  },
  errorInput: {
    color: Colors.danger,
    marginTop: 2,
    fontSize: 10
  }
});
