import * as Yup from 'yup';
import {emailValidation, passwordValidation} from './CommonValidation';

function getLoginValidationSchema(values) {
  return Yup.object().shape({
    password: passwordValidation,
    passwordConfirm: passwordValidation.oneOf(
      [values.password],
      'Password must be same!'
    ),
    email: emailValidation
  });
}

function getErrorsFromValidationError(validationError) {
  const FIRST_ERROR = 0;
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR]
    };
  }, {});
}

export default function validateLoginForm(values) {
  const validationSchema = getLoginValidationSchema(values);
  try {
    validationSchema.validateSync(values, {abortEarly: false});
    return {};
  } catch (error) {
    return getErrorsFromValidationError(error);
  }
}
