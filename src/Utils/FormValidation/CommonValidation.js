import * as Yup from 'yup';

const passwordValidation = Yup.string()
  .min(8, 'Minimum password length is 8')
  .required('Required');
const emailValidation = Yup.string()
  .email('Wrong email format')
  .required('Required');

export {passwordValidation, emailValidation};
